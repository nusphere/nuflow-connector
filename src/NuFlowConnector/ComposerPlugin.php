<?php

namespace NuFlowConnector;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;

class ComposerPlugin implements PluginInterface, EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return [ScriptEvents::POST_INSTALL_CMD => 'dumpVersionsClass'];
    }


    public static function dumpVersionsClass(Event $composerEvent)
    {
        $composer    = $composerEvent->getComposer();
        $rootPackage = $composer->getPackage();

        $composerEvent->getIO()->write('<info>nu-box/nu-flow-connector:</info> sending composer info...');


        $composerEvent->getIO()->write('<info>nu-box/nu-flow-connector:</info> ... completed');
    }

    public function activate(Composer $composer, IOInterface $io)
    {
        // TODO: Implement activate() method.
    }

    public function deactivate(Composer $composer, IOInterface $io)
    {
        // TODO: Implement deactivate() method.
    }

    public function uninstall(Composer $composer, IOInterface $io)
    {
        // TODO: Implement uninstall() method.
    }
}